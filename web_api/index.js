// Imports express as per the "require" keyword
var express = require('express');
// Creates an express application
var app = express();
// Module for reading files
var fs = require("fs");

// API endpoint
app.get('/api/coders', function (req, res) {
    // NOTE: __dirname reference the directory where the JS file resides
   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
      // Retrieve query parameter if it exists 
      let id = req.query.id;
      // Convert file read (which is a string) into JSON format 
      let jsonData = JSON.parse(data);
      if(id == undefined){
         // if no query parameter exists, return all users 
         res.status(200)
         res.json(jsonData.users);
      }
      else{
         // else return the user specified by 'id' if one exists
         if(id < jsonData.users.length){
            res.status(200)
            res.json(jsonData.users[id]);
         }
         else{
            // no user with specified id
            let errorMsg = '{"message" : "No user with specified ID found." }';
            res.status(404)
            res.json(JSON.parse(errorMsg));
         }
      }
   });
});
// For all other requests, send a 404 response
app.get('/api/*', function (req, res){
   let errorMsg = '{"message" : "Resource not found." }';
   res.status(404);
   res.json(JSON.parse(errorMsg));
});
// Start server with port and callback function that triggers once server is
// started 
var server = app.listen(8081, function () {
   var port = server.address().port
   console.log("Client listening on port: %s", port)
})