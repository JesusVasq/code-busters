# Project Documentation For Company Website

### List of Tools Used for Project:

1. Docker
2. React
3. Docker-Compose
4. ExpressJs
5. Nginx
6. (More to be Added)

### Folder Structure for Project:

- code-busters
  - back_end
  - docs
  - front_end

### Steps to Start Project:

**Step 1:** Get extensions on Visual Studio Code (If VS Code is being used) that help such as **_Remote - WSL_** (If coding in windows with a Linux subsystem), **_Debugger for Chrome_** (Or any desired Browser), **_Docker_** in order to make actions easier for future work.

**Step 2:** Get Docker setup on machine, by following the [Docker Setup Steps For Linux](https://docs.docker.com/engine/install/ubuntu/). If you are running WSL, then Docker Desktop must be downloaded for Windows. If the WSL is not version 2, then the following commands must be ran in order to get WSL to version 2:

```
wsl -l -v

Get your name and substitutie for the bottom in distro name
wsl --set-version (distro name) 2
```

**Step 3:** After installing Docker and runnning the commands neccessary to check if it is working, then a React App must be created next to have the React Library and corresponding components installed. Make sure to install npm(Node Package Manager) and NodeJs using the following commands:

```
sudo apt update

sudo apt install nodejs npm

Check Version:
nodejs -v

Check Version:
npm -v
```

Once the previous commands have been installed, then the following commands can be ran to get a react app running on Port 3000 which will be done with the following commands to work on developmental server:

```
npx create-react-app my-app
cd my-app
npm start
```

**Step 4:** After testing the enviornmental server to see if the sample React App worked, a simple server must be built in order to run a production build in the further steps. The following must be performed to create a simple ExpressJs server:

```
cd back_end
npm init
npm install express --save
```

Please notice that **_npm init_** will create a package.json file that will be needed for the server so it can run correctly. The **_npm install express --save_** command will install ExpressJs as well as the package-lock.json file needed for the server. Once the previous commands have been ran in the back_end folder, the following code can be written to set up a simple server (**Note:** The name of this file where the code will be written is index.js; change name as needed).

```
const express = require('express');
const app = express();
const port = 8080;

const path = require('path');

app.use(express.static(path.join(__dirname, '../front_end/build')));

app.get('/', (req, res) => {
	res.sendFIle(path.join(__diranme, '../front_end/build', 'index.html'));
});

app.listen(port);

console.log('Client listening on port: ', port);
```

Once the previous is written, then the production build can be ran through ExpressJs. The following has to be performed in order to get the server up and runnning in the specified port:

```
cd front_end
npm run-script build
cd ..
cd back_end
node index.js
```

Please notice that **_npm run-script build_** will build all the files that were modified in the source folder so that the production build can be up to date with latest changes before running it up in the server. The **_node index.js_** command will start running the server that was written in the file index.js, which will run on localhost with the port specified. (**Note:** Please make sure that there are **node_modules** folders in each back*end and front_end folder. If there is no node module folders, then run the command \*\*\_npm install*\*\* in each folder to install the modules.)

**Step 5:** The next step is to create a new branch in GitLab to develop on rather than working straight on main branch. Make sure to create a **"develop"** branch on GitLab, then on your local machine run the command **_git fetch_** to get the newly created branch from remote origin, and then use the command **_git checkout develop_** to create a local branch that can be worked on. Once any changes are done in develop, it can be pushed to the remote branch and later on, develop branch can be merged into main branch. If there is any other questions on git related commands please check out this cheat sheet: [Git Commands Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)

**Step 6:** The next step is to create an Amazon Web Services(AWS) or Google Cloud Platform(GCP) instance to run the application on. In this example we are going to create an AWS instance through Amazon's "Compute EC2" option. This would be created as an Ubuntu image. Once the instance is created, the command **_git clone (GitLab repo name)_** must be used to pull the code into the machine and then step 2 and step 3 can be ran (In step 3, exclude creating a react app, as the pulled code already has the working code). Once everything is set up in the machine, step 4 can be followed to run the server in the instance, and use the IP Address of the machine, alongside with the the specified port, to check the running build. Since the server would terminate when the instance is exited, it can be running in the background of the instance using **PM2**, which can be installed and used with the following commands:

```
npm install -g pm2
pm2 start index.js
pm2 stop index.js
```

Please notice that **_npm install -g pm2_** install the neccessary process manager that is needed to keep the app running in the background. The command **_pm2 start index.js_** is to start the process, whereas the other command is to stop it.

**Step 7:** The next step is to create a basic **_DockerFile_** and it's configurations needed to run docker images and containers. The following code can be written for a basic DockerFile:

```
 FROM node:latest

WORKDIR /app

COPY . .

WORKDIR /app/front_end
RUN npm install && npm run-script build
WORKDIR /app/back_end
RUN npm install
WORKDIR /app/back_end

EXPOSE 8080

CMD [ "node", "index.js" ]
```

After creating the DockerFile, the commands from [Docker Instructions](docker_instructions.md) or this [Docker Cheat Sheet](https://www.docker.com/sites/default/files/d8/2019-09/docker-cheat-sheet.pdf) to maneuver through creating containers and images.

**Step 8:** The next step is to create a "docker-compose.yml" file to easily create and start all the services from your from the yaml configurations that were set with a single command. Usually, compose consists of a three-step process which are below:

1. Define your app’s environment with a Dockerfile so it can be reproduced anywhere.

2. Define the services that make up your app in docker-compose.yml so they can be run together in an isolated environment.

3. Run docker-compose up and Compose starts and runs your entire app.

The following code is a basic setup of a docker-compose.yml file, which can be modified or changed as needed for preference.

```
version: '3.8'

services:

  code_busters_web:

    build: .

    image: code_busters_image

    ports: ['8080:8080']

    volumes:
      - .:/app
```

The simple commmand that brings your application up with the container and image is **_docker-compose up -d_** and the one that brings it down and deletes everything is **_docker-compose down_**. If a more comprehensive guide is wanted for working with docker-compose, then this [docker-compose cheat sheet](https://jstobigdata.com/docker-compose-cheatsheet/) is a good look.

**Step 9:** This step is to set up the GitLab pipeline using a ".gitlab-ci.yml" file which will set up the test scripts for the automation in GitLab to run. If the tests are passed, then the code should deploy and run correctly, however, if there is an error, then the pipeline would let user know of this.
