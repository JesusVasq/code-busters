Docker Manual
================================================================================
## Building the image
To build the image run the following command
```
docker build -t <NAME_OF_IMAGE> .
```
## Listing images 
Make sure the newly built image is listed after running the command below 
```
docker images 
```
## Create container
To create a container ( run the image ) use the following below 
```
docker run -p 8080:8080 -d --name <NAME_OF_CONTAINER> <NAME_OF_IMAGE>:latest
```
The parameters used do the following 
- `-p 8080:8080` Publishes the port specified in the docker file the the **HOST** machine
- `-d` Detaches the terminal used to run docker container from the container's standard output
## Check the container
Verify the container is running by using the following command below
```
docker ps
``` 
## Stopping the container 
Stop the running container using the following command below 
```
docker stop <NAME_OF_CONTAINER>
```

## Deleting the container
After stopping the container verify that the container exited correctly by running
```
docker ps -a
```
After verifying your docker container has stopped, you can restart the container using
```
docker start <NAME_OF_CONTAINER>
```
You can also delete the container by running 
```
docker container prune
```
This command will delete all docker containers that have been stopped
