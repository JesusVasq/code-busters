echo "Generating certificate and private key..."

openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout key.pem -out cert.pem -config req.conf -extensions 'v3_req'
# RSA: algorithm used to generate private key 
# 4096: key size in bits 
# 365: validity duratrion of cert and key 
# -config req.conf: certificate request configuration