// Imports express as per the "require" keyword
const express = require('express');
// Creates an express application
const app = express();
// Establishes the port that the app will listen to
const port = 8080;
// Used for handling and transforming file paths
const path = require('path');

// A middleware function that is used to render static css and javascript files
/*	NOTE: When running 'npm build' in the 'front_end' directory, the 'build'
	folder is created with sub-directories where static assets are stored in their 
	respective folders like 'css', 'images', 'javascript', etc... For express to 
	utilize these assets, this middleware function must be run. */
app.use(express.static(path.join(__dirname, '../front_end/build')));

// This middleware function listens to all GET requests
app.get('/', (req, res) => {
	// path join combines all the arguments into a single path 
	// NOTE: Since React is an SPA (Single-Page Application), we only need to 
	// serve one HTML file which is the index.js generated from 'npm build'
	res.sendFile(path.join(__dirname, '../front_end/build', 'index.html'));
});

// Port application will run on 
app.listen(port);

console.log('Client listening on port: ', port);
