import React from "react";
import { AppBar, Toolbar, Typography, IconButton, Button} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme) => ({
    header: {
        backgroundColor: "#000000",
    },
    title: {
    },
    hamburgerMenu:{
        display: "none",
        [theme.breakpoints.down('xs')]:{
            display: "inline"
        }
    },
    toolbar: {
        display: "flex",
    },
    buttons: {
        [theme.breakpoints.down('xs')]: {
            display: "none",
        },
    },
}));

export default function Header(){

    const classes = useStyles();

    return(
        <header>
            <AppBar className={classes.header}>
                <Toolbar className={classes.toolbar} spacing={24}>
                    <IconButton edge="start" color="inherit" aria-label="menu" className={classes.hamburgerMenu} md={8}>
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h4" component="h1" md={8}>CodeBusters</Typography>
                    <div className={classes.buttons} md={8}>    
                        <Button color="inherit">About</Button>
                        <Button color="inherit">Projects</Button>                    
                        <Button color="inherit">Contact</Button> 
                    </div>                      
                </Toolbar>
            </AppBar>
        </header>
    )
}